## Installation

1. Clone this repo.
2. cd into directory and run `yarn`
3. Run `gulp build` to compile SASS, JS & create copy image
4. Run `gulp` to browsync your JS, SASS & HTML
