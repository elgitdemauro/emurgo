var gulp = require('gulp'),
		sass = require ('gulp-sass'),
		notify = require('gulp-notify'),
		filter = require('gulp-filter'),
		autoprefixer = require('gulp-autoprefixer'),
		concat = require('gulp-concat'),
		uglify = require('gulp-uglify'),
		imagemin = require('gulp-imagemin');

		browserSync = require('browser-sync'),
		reload 		= browserSync.reload;
		
var PATH = {
	stylesPath: 'assets/sass',
	imagesPath: 'assets/img',
	jsPath: 'assets/js',
	outputDir: 'public/dist',
	htmlPublic: 'public'
}


gulp.task('icons', function() { 
	return gulp.src('./node_modules/font-awesome/fonts/**.*') 
		.pipe(gulp.dest(PATH.outputDir + '/fonts')); 
});

gulp.task('images', function() { 
	return gulp.src(PATH.imagesPath + '/**.*')
		.pipe(imagemin())
		.pipe(gulp.dest(PATH.outputDir + '/img'))
});

gulp.task('css', function() {
	return gulp.src(PATH.stylesPath + '/main.scss')
		.pipe(sass({
				outputStyle: 'compressed',
				noCache: true,
				includePaths: [
					PATH.stylesPath,
					'./node_modules/bootstrap/scss',
					'./node_modules/font-awesome/scss'
				]
			}).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(gulp.dest(PATH.outputDir + '/css'))
		.pipe(reload({stream:true}));
});

gulp.task('fonts', function(){
	return gulp.src('./assets/fonts/**.*') 
		.pipe(gulp.dest(PATH.outputDir + '/fonts')); 
});

gulp.task('jquery', function(){
	return gulp.src('./node_modules/jquery/dist/jquery.min.js') 
		.pipe(gulp.dest(PATH.outputDir + '/js')); 
});

gulp.task('bootstrap-js', function(){
	return gulp.src('./node_modules/bootstrap/dist/js/bootstrap.min.js') 
		.pipe(gulp.dest(PATH.outputDir + '/js')); 
});

gulp.task('popper', function(){
	return gulp.src('./node_modules/popper.js/dist/popper.min.js') 
		.pipe(gulp.dest(PATH.outputDir + '/js')); 
});

gulp.task('js', function() {
	return gulp.src(PATH.jsPath+'/**.*')
		// .pipe(filter('**/*.js'))
		// .pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest(PATH.outputDir + '/js'))
		pipe(reload({stream:true}));
});

gulp.task('browser-sync', function() {
    browserSync({
    	logLevel: "info",
    	logConnections: true,
    	notify: false,
        server: {
            baseDir: PATH.htmlPublic
        }
    });
});

gulp.task('refresh', function () {
    browserSync.reload();
});

gulp.task('build', ['icons', 'images', 'fonts','css', 'jquery', 'bootstrap-js', 'js', 'popper']);

gulp.task('default', ['browser-sync'], function(){
	gulp.watch([PATH.stylesPath + '**/*.scss'], ['css']);
	gulp.watch([PATH.jsPath + '**/*.js'], ['js']);
	gulp.watch([PATH.imagesPath + '/**/*'], ['images']);
	gulp.watch([PATH.htmlPublic + '/*.html'], ['refresh']);
});
