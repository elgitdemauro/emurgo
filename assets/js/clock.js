// CLOCK 
var hourHand_sp = document.querySelector('.hour-hand.sp');
var hourHand_jp = document.querySelector('.hour-hand.jp');
var hourHand_vn = document.querySelector('.hour-hand.vn');
var hourHand_uk = document.querySelector('.hour-hand.uk');
var hourHand_hk = document.querySelector('.hour-hand.hk');
var minuteHand_sp  = document.querySelector('.min-hand.sp');
var minuteHand_jp  = document.querySelector('.min-hand.jp');
var minuteHand_vn  = document.querySelector('.min-hand.vn');
var minuteHand_uk  = document.querySelector('.min-hand.uk');
var minuteHand_hk  = document.querySelector('.min-hand.hk');

var time = new Date();
var minutes = time.getMinutes();

var stgo_h = time.toLocaleString('en-US', {
                              timeZone: 'America/Santiago', 
                              hour: '2-digit',
                              hour12: false
                            });
var sp_h = time.toLocaleString('en-US', {
                              timeZone: 'Asia/Singapore', 
                              hour: '2-digit',
                              hour12: false
                            });
   
var jp_h = time.toLocaleString('en-US', {
                              timeZone: 'Asia/Tokyo', 
                              hour: '2-digit',
                              hour12: false
                            });   
var vn_h = time.toLocaleString('en-US', {
                              timeZone: 'Asia/Ho_Chi_Minh', 
                              hour: '2-digit',
                              hour12: false
                            }); 
var uk_h = time.toLocaleString('en-US', {
                              timeZone: 'Europe/London', 
                              hour: '2-digit',
                              hour12: false
                            });
var hk_h = time.toLocaleString('en-US', {
                              timeZone: 'Asia/Hong_Kong', 
                              hour: '2-digit',
                              hour12: false
                            });   


var base = 90;
var _hours_sp = base + 360 / 12 * sp_h;
var _hours_jp = base + 360 / 12 * jp_h;
var _hours_vn = base + 360 / 12 * vn_h;
var _hours_uk = base + 360 / 12 * uk_h;
var _hours_hk = base + 360 / 12 * hk_h;
var _minutes = base + 360 / 60 * minutes;

var tick = function tick() {
  _hours_sp += 360 / 60 / 60 / 12;
  _hours_jp += 360 / 60 / 60 / 12;
  _hours_vn += 360 / 60 / 60 / 12;  
  _hours_uk += 360 / 60 / 60 / 12;
  _hours_hk += 360 / 60 / 60 / 12;
  _minutes  += 360 / 60 / 60;
  
  hourHand_sp.style.transform = 'rotate(' + _hours_sp + 'deg)';
  hourHand_jp.style.transform = 'rotate(' + _hours_jp + 'deg)';
  hourHand_vn.style.transform = 'rotate(' + _hours_vn + 'deg)';
  hourHand_uk.style.transform = 'rotate(' + _hours_uk + 'deg)';
  hourHand_hk.style.transform = 'rotate(' + _hours_hk + 'deg)';

  minuteHand_sp.style.transform = 'rotate(' + _minutes + 'deg)';   
  minuteHand_jp.style.transform = 'rotate(' + _minutes + 'deg)';   
  minuteHand_vn.style.transform = 'rotate(' + _minutes + 'deg)';   
  minuteHand_uk.style.transform = 'rotate(' + _minutes + 'deg)';   
  minuteHand_hk.style.transform = 'rotate(' + _minutes + 'deg)';   
};

setInterval(tick, 1000);


