// NAVBAR
$(document).ready(function(){
  $(window).bind('scroll', function() {
    var navHeight = $('.top').height();
    if ($(window).scrollTop() > navHeight) {  
      console.log(navHeight);    
      $('.logo').removeClass('hide');   
      $('.navbar').addClass('navbar-fixed');
      $('.logo-white').addClass('hide');
      $('.dropdown-menu').addClass('bg');
     }
    else {
      $('.navbar').removeClass('navbar-fixed');
      $('.logo').removeClass('hide');
      $('.dropdown-menu').removeClass('bg');
      $('.logo-color').addClass('hide');
     }
  });
});




// 
$('#next').click(function(){  
  $('.form-primary-step').hide();
  $('.form-second-step').fadeIn();
})
$('#back').click(function(){  
  $('.form-second-step').hide();
  $('.form-primary-step').fadeIn();
})




// PARALLAX JUMBOTRON
window.addEventListener("scroll", function(){
  var parallax = document.getElementById("jumbotron");
  var imgZoom = document.getElementsByClassName("imgZoom");
  var offset = window.pageYOffset;
  parallax.style.backgroundPositionY = 
    offset *  0.7 + "px" ;
})




// TABS
function tabs(value) {  
  var i;
  var x = document.getElementsByClassName("content-box");
  var menu = document.getElementsByClassName("menu");

  for (i = 0; i < x.length; i++) {
     x[i].classList.remove("show");  
     menu[i].classList.remove("active");  
  }
  document.getElementById(value).classList.add("show");  
}



// TABS NEWS FILTER 
$(function() {
  var selectedClass = "";
  $(".btn-tab").click(function(){ 

    selectedClass = $(this).attr("data-rel"); 
     $("#portfolio").fadeTo(100, 0.1);
    $("#portfolio .tile").not("."+selectedClass).fadeOut().removeClass('scale-anm');
    setTimeout(function() {
      $("."+selectedClass).fadeIn().addClass('scale-anm');
      $("#portfolio").fadeTo(300, 1);
    }, 300); 
    
  });
});


















